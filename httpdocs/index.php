<?php

require_once __DIR__ . '/../src/calculator.php';
require_once __DIR__ . '/../src/error/appLogicErrorLib.php';
require_once __DIR__ . '/../src/view/templateEngine.php';

$calculatorResult = runCalculator();

showcalculatorMain($calculatorResult);
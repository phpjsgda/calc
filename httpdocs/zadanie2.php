<?php

$numbers = [1.1,0.0,3.3,4.4,5.5];

function changeTable1(array $numbers)
{
    $sum = 0;
    $newArray = [];
    foreach ($numbers as $var) {
        if ($var > 1 && $var < 5) {
            $sum += toInt($var);
            $newArray[] = $var;
        }
    }
    echo $sum;
    return $newArray;
}

function changeTable2(array $numbers){
    $sum = 0;
    $ile=count($numbers)-1;
    for($i=0;$i<$ile;$i++){
        $var = $numbers[$i];
        if($var > 1 && $var < 5) {
            $sum += toInt($var);
        }else{
            unset($numbers[$i]);
        }
    }
    echo $sum;
    return $numbers;
}

function changeTable3(array $numbers){
    $sum = 0;
    foreach($numbers as $key =>$var){
        if($var > 1 && $var < 5) {
            $sum += toInt($var);
        }else{
            unset($numbers[$key]);
        }
    }
    echo $sum;
    return $numbers;
}
function toInt($float){
    return (int)$float;
}

function showTable(array $arr){
    echo '<ul>';
    foreach ($arr as $value) {
        echo '<li>' . $value . '</li>';
    }
    echo '</ul>';
}

showTable(changeTable1($numbers));
echo '<br>';
$newNumbers = changeTable2($numbers);
showTable($newNumbers);
echo '<br>';
$newNumbers = changeTable3($numbers);
showTable($newNumbers);

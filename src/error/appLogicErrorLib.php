<?php

$GLOBALS['errors'] = [];

/**
 * @param string $error
 */
function addAppError($error)
{
    $GLOBALS['errors'][] = $error;
}

/**
 * @return bool
 */
function appHasErrors()
{
    return (array_key_exists('errors', $GLOBALS) && count($GLOBALS['errors']) > 0);
}

/**
 * @return array
 */
function getAppErrors(){
    if (appHasErrors()){
        return $GLOBALS['errors'];
    }
    return [];
}
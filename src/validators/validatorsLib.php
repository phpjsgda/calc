<?php

require_once __DIR__ . '/../config/dictionary.php';
require_once __DIR__ . '/../error/appLogicErrorLib.php';

/**
 * @param string $firstVar
 * @param string $secondVar
 * @param string $operation
 * @return bool
 */
function validateData($firstVar, $secondVar, $operation)
{
    $isValid = true;

    if (false === validateOperator($operation)) {
        $isValid = false;
    }

    if (false === validateVariable($firstVar, FIRST_DATA_ALIAS)) {
        $isValid = false;
    }

    if ((OPERATOR_DIVIDE === $operation && false === validateVariableNonZero($secondVar, SECOND_DATA_ALIAS)) ||
        (OPERATOR_DIVIDE !== $operation && false === validateVariable($secondVar, SECOND_DATA_ALIAS))
    ) {
        $isValid = false;
    }

    return $isValid;
}

/**
 * @param string $operator
 * @return bool
 */
function validateOperator($operator)
{
    static $availableOperations = [
        OPERATOR_PLUS,
        OPERATOR_MINUS,
        OPERATOR_DIVIDE,
        OPERATOR_MULTIPLY
    ];

    if (false === in_array($operator, $availableOperations, false)) {
        addAppError(REQUEST_DATA_INCORRECT . OPERATOR_ALIAS);
        return false;
    }

    return true;
}

/**
 * @param string $data
 * @param string $varName
 * @return bool
 */
function validateVariable($data, $varName)
{
    if (!is_numeric($data)) {
        addAppError(REQUEST_DATA_INCORRECT . $varName);
        return false;
    }

    return true;
}

/**
 * @param string $data
 * @param string $varName
 * @return bool
 */
function validateVariableNonZero($data, $varName)
{
    if (false === validateVariable($data, $varName)) {
        return false;
    }

    if (0.0 === (float)$data) {
        addAppError(sprintf(REQUEST_DATA_CANT_BE_ZERO_OR_EMPTY, $varName));
        return false;
    }

    return true;
}
<?php

require_once __DIR__ . '/../config/dictionary.php';

/**
 * @return bool
 */
function requestHasData()
{
    return (bool)count($_REQUEST);
}

/**
 * @param $varName
 * @return string|null
 */
function getDataFromRequest($varName)
{
    if (false === array_key_exists($varName, $_REQUEST)) {
        addAppError(REQUEST_DATA_MISSING . $varName);
        return null;
    }
    return $_REQUEST[$varName];
}

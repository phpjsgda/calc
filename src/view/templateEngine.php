<?php

function showcalculatorMain(array $templateVars)
{
    $template = loadTemplate('calculatorMain.tmpl.html');

    $template = str_replace('[[result]]', $templateVars['result'], $template);
    $template = str_replace('[[var1]]', $templateVars['var1'], $template);
    $template = str_replace('[[var2]]', $templateVars['var2'], $template);

    echo $template;
}

/**
 * @param string $templateName
 * @return string
 */
function loadTemplate($templateName){
    return file_get_contents(__DIR__ . '/templates/' . $templateName);
}
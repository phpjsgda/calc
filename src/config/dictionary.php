<?php
define('REQUEST_DATA_MISSING', 'Nie przekazano wszystkich wymaganych danych. Brak : ');
define('REQUEST_DATA_INCORRECT', 'Przekazano niepoprawne dane dla ');
define('REQUEST_DATA_CANT_BE_ZERO_OR_EMPTY', 'Zmienna %s nie może przyjmować wartości zero dla wybranej operacji');
define('TEMPLATE_FILE_LOAD_ERROR', 'Nie moge załadować szablonu strony');
define('WAITING_FOR_CALCULATION_DATA', 'Czekam na dane do obliczeń');

define('FIRST_DATA_ALIAS', 'A');
define('SECOND_DATA_ALIAS', 'B');
define('OPERATOR_ALIAS', 'RODZAJ OPERAJI');

define('OPERATOR_PLUS', '+');
define('OPERATOR_MINUS', '-');
define('OPERATOR_DIVIDE', '/');
define('OPERATOR_MULTIPLY', '*');


<?php

require_once __DIR__ . '/config/config.php';
require_once __DIR__ . '/config/dictionary.php';
require_once __DIR__ . '/request/requestToolsLib.php';
require_once __DIR__ . '/validators/validatorsLib.php';

/**
 * @return array
 */
function runCalculator()
{
    $result = [
        'result' => '',
        'var1' => '',
        'var2' => ''
    ];

    if (true === requestHasData()) {

        $operator = getDataFromRequest('operation');
        $result['var1'] = getDataFromRequest('var1');
        $result['var2'] = getDataFromRequest('var2');

        if (true === validateData($result['var1'], $result['var2'], $operator)) {
            $result['result'] = calculate(
                $operator,
                (float)$result['var1'],
                (float)$result['var2']
            );
        }
    }

    return $result;
}

/**
 * @param string $operation
 * @param int $var1
 * @param int $var2
 * @return string
 */
function calculate($operation, $var1, $var2)
{
    $result = '';

    switch ($operation) {
        case OPERATOR_PLUS:
            $result = $var1 + $var2;
            break;
        case OPERATOR_MINUS:
            $result = $var1 - $var2;
            break;
        case OPERATOR_MULTIPLY:
            $result = $var1 * $var2;
            break;
        case OPERATOR_DIVIDE:
            $result = $var1 / $var2;
            break;
    }

    return (string)$result;
}